package main

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

//Struct Barang (Model)

type Barang struct {
	ID         string    `json:"id"`
	KodeBarang string    `json:"kodebarang"`
	NamaBarang string    `json:"namabarang"`
	Stok       string    `json:"stok"`
	Produsen   *Produsen `json:"produsen"`
}

//Struct Produsen

type Produsen struct {
	Perusahaan string `json:"perusahaan"`
	Alamat     string `json:"alamat"`
	Telepon    string `json:"telepon"`
}

//Init Barang var as a slice Baranag Struct
var barangs []Barang

//Get All Barang

func getBarang(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(barangs)
}

//Get Barang by ID

func getBarangId(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r) //GET Params
	//Loop Through Barang and Find it
	for _, item := range barangs {
		if item.ID == params["id"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&Barang{})
}

//Create Barang

func createBarang(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var barang Barang
	_ = json.NewDecoder(r.Body).Decode(&barang)
	barang.ID = strconv.Itoa(rand.Intn(1000000))
	barangs = append(barangs, barang)
	json.NewEncoder(w).Encode(barang)
}

//Update Barang

func updateBarang(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for index, item := range barangs {
		if item.ID == params["id"] {
			barangs = append(barangs[:index], barangs[index+1:]...)
			var barang Barang
			_ = json.NewDecoder(r.Body).Decode(&barang)
			barang.ID = params["id"]
			barangs = append(barangs, barang)
			json.NewEncoder(w).Encode(barang)
			return
		}
	}
	json.NewEncoder(w).Encode(barangs)
}

//Delete Barang

func deleteBarang(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	for index, item := range barangs {
		if item.ID == params["id"] {
			barangs = append(barangs[:index], barangs[index+1:]...)
			break
		}
	}
	json.NewEncoder(w).Encode(barangs)
}

func main() {
	//init Router
	router := mux.NewRouter()

	//Mock Data -todo DB
	barangs = append(barangs, Barang{ID: "1", KodeBarang: "358", NamaBarang: "Susu Indomilk", Stok: "50", Produsen: &Produsen{Perusahaan: "PT Indolakto", Alamat: "Jakarta", Telepon: "021-996545"}})
	barangs = append(barangs, Barang{ID: "2", KodeBarang: "423", NamaBarang: "Kecap Bango", Stok: "100", Produsen: &Produsen{Perusahaan: "PT Unilever", Alamat: "Bekasi", Telepon: "021-235445"}})
	//Router Handler
	router.HandleFunc("/api/barang", getBarang).Methods("GET")
	router.HandleFunc("/api/barang/{id}", getBarangId).Methods("GET")
	router.HandleFunc("/api/barang", createBarang).Methods("POST")
	router.HandleFunc("/api/barang/{id}", updateBarang).Methods("PUT")
	router.HandleFunc("/api/barang/{id}", deleteBarang).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8080", router))
}
